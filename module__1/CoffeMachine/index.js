// TODO  Реализовать функционал кофе машины -
//* 1) Создать обьект кофе машины с такими свойствами - name(название кофемашинк), water(колл-во воды в машине), grains(колл-во кофе в машине), drink(массив обьектов значений кофе которые машина может приготовить и их цена), sugar(колл-во сахара в машине).
//* 2) В данном обьекте написать методы приготовления кофе
//* 3) Получить значение с полей для ввода, в которые пользователь запишет какой напиток он хочет и сколько денег он заплатит
//* 4) Сделать кофе!!!!)))

const coffeeMachine = {
	name: 'arabika',
	water: 10,
	grains: 100,
	drinks: [
		{ coffeeName: 'capuccino', price: 20, water: 0.8, grains: 10 },
		{ coffeeName: 'latte', price: 25, water: 1, grains: 13 },
		{ coffeeName: 'espresso', price: 15, water: 0.4, grains: 5 },
	],
	sugar: 100,
	capuccino: function (userPrice) {
		const singleDrink = filterDrinks('capuccino')

		if (userPrice < singleDrink.price) {
			alert('не хватает денег')
		} else {
			this.water = this.water - singleDrink.water
			this.grains = this.grains - singleDrink.grains
			alert(`Ваша сдача - ${userPrice - singleDrink.price}`)
		}
	},
	latte: function (userPrice) {
		const singleDrink = filterDrinks('latte')

		coffeeMachine.water = coffeeMachine.water - singleDrink.water
		this.grains = this.grains - singleDrink.grains
	},
	espresso: function (userPrice) {
		const singleDrink = filterDrinks('espresso')

		coffeeMachine.water = coffeeMachine.water - singleDrink.water
		this.grains = this.grains - singleDrink.grains
	},

	filterDrinks(name) {
		const result = coffeeMachine.drinks.filter(function (item) {
			return item.coffeeName === name
		})
		return result[0]
	},
}

const coffeeName = document.querySelector('.coffee-name')
const coffeePrice = document.querySelector('.price')
const order = document.getElementById('submit')

order.addEventListener('click', function (e) {
	e.preventDefault()
	const drinkName = coffeeName.value
	const drinkPrice = coffeePrice.value
	// coffeeMachine[drinkName](drinkPrice)
	// console.log(coffeeName.classList.contains('coffee-name'))
})

// console.log(coffeeMachine['name'])

// const studentList = [
// 	'Арсений Шпорта',
// 	'Давид Васьковський',
// 	'Серегей Найденко',
// 	'Лёша Буглак',
// 	'Саша Колачко',
// 	'Даня Гресько',
// 	'Татур Егор',
// 	'Настя Кривовяз',
// 	'Никита Примський',
// 	'Егор Сторчило',
// 	'Саша Скляров',
// 	'Гарик',
// 	'Тимофей',
// 	'Настя Гурбик',
// ]

// const listNumbers = [1, 2, 3, 4]

// const newList = studentList.slice(5)
// console.log('🚀 ==== > newList', newList)
// const everyMethods = listNumbers.every(function (item) {
// 	return item > 3
// })
// const filterMethods = listNumbers.filter(function (item) {
// 	return item > 3
// })
// const reduceMethods = listNumbers.reduce(function (accumulator, singleItem) {
// 	return accumulator + singleItem
// })

// console.log('🚀 ==== > everyMethods', reduceMethods)

// const newarr = studentList.concat(listNumbers)
// const newarr = [...studentList, ...listNumbers]

// console.log(newarr)
// const randomNumber = Math.floor(Math.random() * studentList.length)
// console.log(studentList[randomNumber])

// console.log('first glpbalnaya napisaniy', this)

// const object = {
// 	name: 'gogi',

// 	summ() {
// 		console.log('v objecte', this)
// 	},
// }

// object.summ()

// function summ(a, b) {
// 	return a + b
// }

// console.log(summ(2, 3))

// const summ1 = (a, b) => a + b
// console.log(summ1(2, 3))

// const arr = [1, 2, 3, 4]

// const filtred = arr.filter(item => item > 1)

// const filtred1 = arr.filter(function (item) {
// 	return item > 1
// })

// let name = prompt('enter youre name')

// while (name !== '') {
// 	let name = prompt('enter youre name')
// }

// for (let i = 0; i < div.length; i++) {
// 	if (status === false) {
// 		break
// 	}

// 	div.style.border = '2px solid blue'
// }

// const drink = 'tea'

// switch (drink) {
// 	case 'ice': // drink === 'ice'
// 		console.log('ice')
// 		break
// 	case 'banana':
// 		console.log('banana')
// 		break
// 	case 'pomme':
// 		console.log('pomme')
// 		break
// }

// if (drink >= 'ice') {
// 	console.log('ice')
// } else if (drink < 'banane') {
// 	console.log('banane')
// } else if (drink === 'pomme') {
// 	console.log('pomme')
// } else if (drink === 'pomme') {
// 	console.log('pomme')
// } else {
// 	console.log('final boss')
// }

// classList:

// -add
// -remove

// - toggle
// - contains
