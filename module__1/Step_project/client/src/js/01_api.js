class Api {
	constructor() {
		this.token = null
	}

	static setToken(id) {
		sessionStorage.setItem('token', id)
		this.token = id
	}

	static getQuery(url, params) {
		const baseUrl = 'http://localhost:8080/api'

		const query = !!params ? baseUrl + url + params : baseUrl + url
		return query
		// let query = ''
		// if (!!params) {
		// 	query = baseUrl + url + params
		// } else {
		// 	query = baseUrl + url
		// }
	}

	static async getRequest(url, params = '') {
		const query = Api.getQuery(url, params)

		const response = await fetch(query, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
			},
		})

		const result = await response.json()

		return result
	}

	static async handleRequest(url, params = '', data, method) {
		const query = Api.getQuery(url, params)

		const response = await fetch(query, {
			method,
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json',
			},
		})

		const result = await response.json()

		return result
	}

	static async deleteRequest(url, params = '') {
		const query = Api.getQuery(url, params)

		const response = await fetch(query, {
			method: 'DELETE',
			headers: {
				'Content-Type': 'application/json',
			},
		})

		const result = await response.json()

		return result
	}
}
