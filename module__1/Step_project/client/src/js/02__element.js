class Element {
	createElement(tag, attr) {
		const element = document.createElement(tag)

		for (let [key, value] of Object.entries(attr)) {
			element[key] = value
		}

		return element
	}
}

// const inputData = {
// 	type: 'email',
// 	required: true,
// 	placeholder: 'Enter email',
// 	className: 'form__email',
// }

// const inputEmail = new Element('input', inputData).createElement()
// console.log('🚀 ==== > inputEmail', inputEmail)

// document.body.prepend(inputEmail)
// const ar = ['hello', 'danit', 'gogi']

// for (let item of ar) {
// 	// console.log(item)
// }

// const obj = {
// 	age: 22,
// 	name: 'gogi',
// }

// const diff = [
// 	['age', '22'],
// 	['name', 'gogi'],
// ]

// for (let item of diff) {
// 	console.log(item)
//     const [key, value] = item
// }
// const difference = Object.entries(obj)

// console.log(obj)
// console.log(difference)
