class Popup {
	constructor(message, status) {
		this.popup = document.querySelector('.popup')
		this.message = message
		this.status = status
	}

	activePopup() {
		const status = this.getStatus()
		const popupText = this.popup.querySelector('.popup__text')
		const popupIcon = this.popup.querySelector('.popup__figure')

		popupText.textContent = this.message

		popupText.style.color = status.colorText

		popupIcon.innerHTML = status.svg

		this.popup.classList.add('active')
		this.popup.style.backgroundColor = status.background

		setTimeout(() => {
			this.popup.classList.remove('active')
		}, 2000)
	}

	getStatus() {
		const statusIcon = {
			success: {
				svg: '<svg xmlns="http://www.w3.org/2000/svg" class="ionicon popup__icon popup__icon--success " viewBox="0 0 512 512"><title>Checkmark Circle</title><path d="M448 256c0-106-86-192-192-192S64 150 64 256s86 192 192 192 192-86 192-192z" fill="none" stroke="currentColor" stroke-miterlimit="10" stroke-width="32"/><path fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="32" d="M352 176L217.6 336 160 272"/></svg>',
				colorText: 'rgb(30, 70, 32)',
				background: 'rgb(237, 247, 237)',
			},

			error: {
				svg: '<svg xmlns="http://www.w3.org/2000/svg" class="ionicon popup__icon popup__icon--error" viewBox="0 0 512 512"><title>Close Circle</title><path d="M448 256c0-106-86-192-192-192S64 150 64 256s86 192 192 192 192-86 192-192z" fill="none" stroke="currentColor" stroke-miterlimit="10" stroke-width="32"/><path fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="32" d="M320 320L192 192M192 320l128-128"/></svg>',
				colorText: 'rgb(95, 33, 32)',
				background: 'rgb(253, 237, 237)',
			},

			warning: {
				svg: '<svg xmlns="http://www.w3.org/2000/svg" class="ionicon popup__icon popup__icon--warning" viewBox="0 0 512 512"><title>Alert Circle</title><path d="M448 256c0-106-86-192-192-192S64 150 64 256s86 192 192 192 192-86 192-192z" fill="none" stroke="currentColor" stroke-miterlimit="10" stroke-width="32"/><path d="M250.26 166.05L256 288l5.73-121.95a5.74 5.74 0 00-5.79-6h0a5.74 5.74 0 00-5.68 6z" fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="32"/><path d="M256 367.91a20 20 0 1120-20 20 20 0 01-20 20z"/></svg>',
				colorText: 'rgb(102, 60, 0)',
				background: 'rgb(255, 244, 229)',
			},
		}
		return statusIcon[this.status]
	}
}
