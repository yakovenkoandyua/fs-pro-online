class Form extends Element {
	render(nameId) {
		const element = this.createElement('form', { id: nameId })

		return element
	}

	serialize(allElementForm) {
		const data = {}
		const childrens = [...allElementForm]

		childrens.forEach(child => {
			if (child.tagName === 'INPUT') {
				data[child.name] = child.value
			}
		})

		return data
		// const childrens = [...allElementForm]
		// const data = childrens.reduce((accumulator, child) => {
		//     if (child.tagName === 'INPUT') {
		//         accumulator[child.name] = child.value
		// 	}
		// }, {})

		// return data
		// const childrens = Array.from(allElementForm)
	}
}

class FormLogin extends Form {
	fields = [
		{
			name: 'name',
			type: 'text',
			placeholder: 'Login',
			minLength: 3,
			className: 'form__field',
			required: true,
			errorText: 'Name field is required',
		},
		{
			name: 'password',
			type: 'password',
			placeholder: 'Enter password',
			minLength: 3,
			className: 'form__field',
			required: true,
			errorText: 'Password field is required',
		},
		{
			type: 'submit',
			value: 'send',
			className: 'form__submit',
		},
	]

	render(htmlWrapper) {
		const { fields } = this

		const form = super.render('form-login')

		form.addEventListener('submit', this.handleSubmit.bind(this))

		fields.forEach(inputData => {
			const input = new Input(inputData).render()
			form.append(input)
		})

		htmlWrapper.append(form)
	}

	async handleSubmit(event) {
		event.preventDefault()
		// const data = new FormData(event.target)
		const childrensField = event.currentTarget.children
		const { name, password } = this.serialize(childrensField)
		const dataField = { name, password }
		const errorElement = document.createElement('p')
		try {
			const result = await Api.handleRequest('/users', '/login', dataField, 'POST')

			if (result?.message === 'invalid password') {
				throw new Error(result.message)
			}

			errorElement.remove()

			Api.setToken(result.token)

			window.location = '/'

			localStorage.setItem(
				'statusPopup',
				JSON.stringify({
					message: 'login success',
					status: 'success',
				}),
			)
		} catch (error) {
			const input = document.querySelector('[name="password"]')

			errorElement.className = 'form__error'
			errorElement.textContent = error.message

			input.after(errorElement)
			// console.error(`${error.name} = ${error.message}`)
		}
	}
}

class FormRegistration extends Form {
	fields = [
		{
			name: 'name',
			type: 'text',
			placeholder: 'Login',
			required: true,
			minLength: 3,
			className: 'form__field',
			errorText: 'Name field is required',
		},
		{
			name: 'email',
			type: 'email',
			placeholder: 'Email',
			required: true,
			minLength: 3,
			className: 'form__field',
			errorText: 'Email field is required',
		},
		{
			name: 'password',
			type: 'password',
			placeholder: 'Enter password',
			required: true,
			minLength: 3,
			className: 'form__field',
			errorText: 'Password field is required',
		},
		{
			name: 'confirmPassword',
			type: 'password',
			placeholder: 'Repeat password',
			required: true,
			minLength: 3,
			className: 'form__field',
			errorText: 'Confirm password field is required',
		},
		{
			type: 'submit',
			value: 'send',
			className: 'form__submit',
		},
	]

	render(htmlWrapper) {
		const { fields } = this

		const form = super.render('form-registr')

		form.addEventListener('submit', this.handleSubmit.bind(this))

		fields.forEach(inputData => {
			const input = new Input(inputData).render()
			form.append(input)
		})

		htmlWrapper.append(form)
	}

	async handleSubmit(event) {
		event.preventDefault()
		// const data = new FormData(event.target)
		const childrensField = event.currentTarget.children
		const { name, password, email } = this.serialize(childrensField)

		const dataField = { name, password, email }
		const errorElement = document.createElement('p')
		try {
			const result = await Api.handleRequest('/users', '/registration', dataField, 'POST')

			if (result?.message === 'User already exists') {
				throw new Error(result.message)
			}

			window.location = '/login.html'
		} catch (error) {
			const input = document.querySelector('[name="confirmPassword"]')

			errorElement.className = 'form__error'
			errorElement.textContent = error.message

			input.after(errorElement)
			// console.error(`This is Error: ${error.name} = ${error.message}`)
		}
	}
}

class FormCreateCard extends Form {
	fields = [
		{
			name: 'name',
			type: 'text',
			placeholder: 'Name hero',
			required: true,
			minLength: 3,
			className: 'form__field',
			errorText: 'Name field is required',
		},
		{
			name: 'role',
			type: 'text',
			placeholder: 'Role hero',
			required: true,
			minLength: 3,
			className: 'form__field',
			errorText: 'Role field is required',
		},
		{
			name: 'attackValue',
			type: 'number',
			// placeholder: 'Enter password',
			required: true,
			className: 'form__field',
			// errorText: 'Password field is required',
			labelText: 'Attack :',
		},
		{
			name: 'hpValue',
			type: 'number',
			// placeholder: 'Enter password',
			required: true,
			className: 'form__field',
			// errorText: 'Password field is required',
			labelText: 'Health :',
		},
		{
			name: 'price',
			type: 'number',
			// placeholder: 'Repeat password',
			required: true,
			className: 'form__field',
			// errorText: 'Confirm password field is required',
			labelText: 'Cost :',
		},
		{
			name: 'effect',
			type: 'text',
			placeholder: 'Enter effect card',
			required: true,
			className: 'form__field',
			errorText: 'Effect field is required',
		},
		{
			name: 'description',
			tag: 'textarea',
			className: 'form__field',
			placeholder: 'Enter description card',
		},
		{
			type: 'submit',
			value: 'send',
			className: 'form__submit',
		},
	]

	render(htmlWrapper) {
		const { fields } = this

		const form = super.render('form-createCard')

		form.addEventListener('submit', this.handleSubmit.bind(this))

		const block = document.createElement('div')
		block.className = 'form__divider'

		fields.forEach(inputData => {
			if (inputData.name === 'description') {
				const { tag, ...data } = inputData
				const element = new Element().createElement(tag, data)

				const errorData = { textContent: 'description must be more 1 word', className: 'form__error' }
				const error = new Element().createElement('p', errorData)

				element.addEventListener('keyup', ev => {
					const checkValue = ev.target.value.split(' ')

					if (checkValue.length <= 1) {
						element.after(error)
					} else {
						error.remove()
						this.setValueCard(ev.target.name, ev.target.value)
					}
				})

				form.append(element)
			} else {
				const input = new Input(inputData).render()

				input.addEventListener('keyup', ev => {
					const name = ev.target.name
					const value = ev.target.value

					this.setValueCard(name, value)
					// this.setValueCard(ev.target.name, ev.target.value)
				})

				switch (input?.children[0]?.name) {
					case 'hpValue':
						block.append(input)
						break

					case 'attackValue':
						block.append(input)
						break

					case 'price':
						block.append(input)
						form.append(block)
						break

					default:
						form.append(input)
						break
				}
			}
		})

		htmlWrapper.append(form)
	}

	async handleSubmit(event) {
		event.preventDefault()
		// const data = new FormData(event.target)
		const childrensField = event.currentTarget.querySelectorAll('input')
		const description = event.currentTarget.querySelector('textarea')

		const fieldData = this.serialize(childrensField)
		const newData = {
			description: description.value,
		}
		for (let key in fieldData) {
			if (key !== '') {
				newData[key] = fieldData[key]
			}
		}

		const errorElement = document.createElement('p')
		try {
			const result = await Api.handleRequest('/card', '', newData, 'POST')

			console.log('🚀 ==== > FormCreateCard ==== > result', result)

			if (!result?._id) {
				throw new Error(result.message)
			}

			const popup = new Popup('card have benn saved', 'success')
			popup.activePopup()
			
			childrensField.forEach(input => {
				input.value = ''
			})
			description.value = ''


		} catch (error) {
			const input = document.querySelector('[name="description"]')

			errorElement.className = 'form__error'
			errorElement.textContent = error.message

			input.after(errorElement)
			// console.error(`This is Error: ${error.name} = ${error.message}`)
		}
	}

	setValueCard(attribute, value) {
		const element = document.querySelector(`[data-value="${attribute}"]`)

		element.textContent = value
	}
}

window.addEventListener('DOMContentLoaded', () => {
	const loginWrapper = document.getElementById('login')

	if (!!loginWrapper) {
		const formLogin = new FormLogin()
		formLogin.render(loginWrapper)
	}

	const registrWrapper = document.getElementById('registr')

	if (!!registrWrapper) {
		const formRegistration = new FormRegistration()
		formRegistration.render(registrWrapper)
	}

	const cardWrapper = document.getElementById('createCard')

	if (!!cardWrapper) {
		const formCard = new FormCreateCard()
		formCard.render(cardWrapper)
	}

	// example 2
	// const createsForm = (id, nameObject) => {
	// 	const wrapper = document.getElementById(id)

	// 	if (!!wrapper) {
	// 		const form = new nameObject()
	// 		form.render(wrapper)
	// 	}
	// }

	// createsForm('login', FormLogin)
	// createsForm('registr', FormRegistration)
	// example 2
})
