class Input extends Element {
	constructor(attr) {
		super()
		this.attr = attr
	}

	render() {
		const { errorText, labelText, ...attr } = this.attr

		this.input = this.createElement('input', attr)

		if (this.attr.required) {
			this.input.addEventListener('focus', this.handleFocus.bind(this))
			this.input.addEventListener('blur', this.handleBlur.bind(this))
		}

		if (labelText) {
			const label = document.createElement('label')
			label.className = 'form__label'
			label.textContent = labelText

			label.append(this.input)

			return label
		}

		return this.input
	}

	handleFocus() {
		if (this.error) {
			this.error.remove()
		}
	}

	handleBlur(event) {
		const { errorText } = this.attr
		if (event.target.value.length <= 0) {
			this.error = this.createElement('p', { textContent: errorText, className: 'form__error' })
			this.input.after(this.error)
		}
	}
}
