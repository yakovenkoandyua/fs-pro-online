;(function handleCmsTabs() {
	const tabsTitleWrapper = document.querySelector('.admin-tabs__list')

	if (!tabsTitleWrapper) return null

	tabsTitleWrapper.addEventListener('click', event => {
		if (event.target === event.currentTarget) return null

		const titleElement = tabsTitleWrapper.querySelector('.active')

		titleElement.classList.remove('active')

		event.target.classList.add('active')

		const tabContent = document.querySelectorAll('.admin-tabs__content-item')

		tabContent.forEach(singleBlock => {
			if (event.target.dataset.title === singleBlock.dataset.content) {
				singleBlock.classList.add('active')
			} else {
				singleBlock.classList.remove('active')
			}
		})
	})
})()
