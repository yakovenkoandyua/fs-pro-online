function carousel() {
	const backgroundImages = [
		'https://mmo-obzor.ru/_pu/1/94060589.jpg',
		'https://img.championat.com/c/1350x759/news/big/d/v/patch-7-29-dlya-dota-2-dobavil-novogo-geroya-izmenil-kartu-i-balans_1618040102852038523.jpg',
		'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSxzcaezxzAQGOqpaVGb0OMbph7ZlmyBIEupw&usqp=CAU',
	]

	const slides = document.querySelectorAll('.carousel__slide')

	slides.forEach((singleSlide, index) => {
		singleSlide.style.backgroundImage = `url(${backgroundImages[index]})`
	})

	let index = 0
	// HOMEWORK
	document.querySelectorAll('.carousel__btn').forEach(item => {
		item.addEventListener('click', () => {
			// code here !!!!
			slides[index].classList.remove('active')
			index++
			if (index >= slides.length) {
				index = 0
			}
			slides[index].classList.add('active')
		})
	})
	// HOMEWORK
}

carousel()
