;(function setAnimationSubtitles() {
	const subtitleItems = document.querySelectorAll('.developers__item')

	subtitleItems.forEach(function (singleItem, index) {
		singleItem.style.animation = `moveSubtitle 5s linear ${index}s`
	})
})()
;(function () {
	const select = document.querySelector('.select')
	const selectHead = document.querySelector('.select__head')
	const selectList = document.querySelector('.select__list')
	const selectValue = document.querySelector('.select__value')

	selectHead &&
		selectHead.addEventListener('click', () => {
			selectList.classList.toggle('active')
			select.classList.toggle('active')
		})

	// code here
	selectList &&
		selectList.addEventListener('click', function (event) {
			selectList.classList.remove('active')
			selectValue.textContent = event.target.textContent
		})

	// const listItems = document.querySelectorAll('.select__list-item')

	// listItems.forEach(singleItems => {
	// 	singleItems.addEventListener('click', (event) => {
	// selectList.classList.remove('active')
	// selectValue.textContent = event.target.textContent
	// })
	// })
	// code here
})()
/************ MODAL ***********/
;(function () {
	const modal = document.getElementById('heroes')
	const modalTitle = document.querySelector('.modal__title')
	const modalImg = document.querySelector('.modal__img img')

	const cardsWrapper = document.querySelector('.cards__pub')

	cardsWrapper &&
		cardsWrapper.addEventListener('click', function (event) {
			const [, hoverBlock] = event.target.closest('.heroes__item').children
			// console.log(hoverBlock.children)
			const [img, title] = hoverBlock.children

			modal.classList.add('active')

			modalTitle.textContent = title.textContent
			modalImg.src = img.src
		})

	const close = document.querySelector('.modal__close')

	close && close.addEventListener('click', () => modal.classList.remove('active'))
})()
/************ MODAL ***********/

/************ TABS ***********/
;(function () {
	const tabsTitle = document.querySelector('.modal__tabs')
	const tabsContents = document.querySelectorAll('.modal__tabs-content__item')

	const choiseTab = ({ target }) => {
		if (target.classList.contains('modal__tabs')) return null

		// 1
		const attr = target.dataset.title
		// 2
		// const attr = target.getAttribute('data-title')

		tabsContents.forEach(function (el) {
			// ex 1
			if (el.dataset.content === attr) {
				el.classList.add('active')
			} else {
				el.classList.remove('active')
			}
			// ex 2***
			el.classList[el.dataset.content === attr ? 'add' : 'remove']('active')
		})

		const activeTitle = document.querySelector('.modal__tabs-item.active')

		activeTitle.classList.remove('active')
		target.classList.add('active')
	}

	tabsTitle && tabsTitle.addEventListener('click', choiseTab)
})()
/************ TABS ***********/

// ex 2***
// el.classList[el.dataset.content === attr ? 'add' : 'remove']('active')
// ============== EXPLAIN ==============
// const obj = {
// 	age: 22,

// 	helloKitty: function(name) {
// 		alert(`Kitty say Hello ${name}`)
// 	},
// 	goodbyeKitty: function(name) {
// 		alert(`Kitty goodbye Hello ${name}`)
// 	}
// }

// obj.helloKitty('Лёша')
// obj.goodbyeKitty('Лёша')

// obj[ 10 > 5 ? 'goodbyeKitty' : 'helloKitty']('Anna')
// ============== EXPLAIN ==============

/************ STATA ***********/
;(function renderStatistic() {
	const dataStatistic = [
		{
			value: 71,
			title: 'games',
		},
		{
			value: 21,
			title: 'wins',
		},
		{
			value: 50,
			title: 'losses',
		},
		{
			value: '38%',
			title: 'Win Rate',
		},
	]

	const tabsItems = [...document.querySelectorAll('.modal__tabs-content__item')]

	let storyItem

	tabsItems?.forEach(element => {
		if (element.dataset.content === 'story') {
			storyItem = element
		}
	})

	dataStatistic.forEach(function (singleBlock) {
		const block = document.createElement('div')
		const valueTitle = document.createElement('h2')
		const text = document.createElement('p')

		const { value, title } = singleBlock

		valueTitle.textContent = value
		text.textContent = title

		block.className = 'story__wrapper'
		valueTitle.className = 'story__wrapper-title'
		text.className = 'story__wrapper-text'

		block.append(valueTitle, text)

		storyItem?.append(block)
	})
})()

/************ STATA ***********/

/************ HANDLEPOPUP ***********/
window.addEventListener('DOMContentLoaded', () => {
	const statusPopup = JSON.parse(localStorage.getItem('statusPopup'))

	if (statusPopup !== null) {
		const { message, status } = statusPopup
		const popup = new Popup(message, status)
		popup.activePopup()
		localStorage.removeItem('statusPopup')
	}
})
/************ HANDLEPOPUP ***********/
