import express, { json, urlencoded } from 'express'
import mongoose from 'mongoose'
import path from 'path'

import config from './src/config.js'
import auth from './src/utils/auth.js'
import cors from 'cors'


import UserRouter from './src/routers/user.route.js'
import CardRouter from './src/routers/card.route.js'

const app = express()

app.use(cors())
app.use(json())
app.use(urlencoded({ extended: false }))
app.use(express.static(path.resolve(path.dirname('')) + '/public/'))
// app.use('*', auth)
// router.get('/', (req, res) => {
// 	res.sendFile(path.join(__dirname, '../../client/dist/index.html'));
// });

app.use('/api/users', UserRouter)
app.use('/api/card', CardRouter)

const PORT = process.env.PORT || config.PORT

mongoose
  .connect(config.DB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log('MongoDb connected'))
  .catch(err => console.error(err))

app.listen(PORT, () => {
  console.log(`Server is running on port: ${PORT}`)
})
