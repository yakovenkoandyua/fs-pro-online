import CardModel from '../models/card.model.js'
import uniqueRandom from 'unique-random'
import _ from 'lodash'
const rand = uniqueRandom(0, 999999)

const addCard = async (req, res) => {
  console.log('🚀 ==== > req', req)

  const productFields = _.cloneDeep(req.body)

  productFields._id = rand()

  const newProduct = new CardModel(productFields)

  newProduct
    .save()
    .then(product => res.json(product))
    .catch(err =>
      res.status(400).json({
        message: `Error happened on server: "${err}" `,
      }),
    )
}

const getCards = async (req, res) => {
  const CardModelDB = await CardModel.find()

  if (CardModelDB) {
    res.send(CardModelDB)
  } else {
    res.status(404)
    req.send({ message: 'there is no such user' })
  }
}

const CardController = {
  getCards,
  addCard,
}
export default CardController
