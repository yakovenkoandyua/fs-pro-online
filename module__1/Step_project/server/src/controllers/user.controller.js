import UserModel from '../models/user.model.js'
import * as bcrypt from 'bcrypt'

import authHelpers, { getToken } from '../utils/authHelpers.js'

const save = async (req, res) => {
  console.log(req.body)
  const { name, password } = req.body
  if (!name || !password) {
    res.status(422)
    res.send({
      message: 'Invalid data. One of the required properties is missing: name, password.',
    })
  } else {
    const userFromDB = await UserModel.findOne({ name })

    if (!userFromDB) {
      const newUser = { ...req.body }
      newUser.password = await bcrypt.hash(newUser.password, 10)

      const savedUser = await UserModel.create(newUser)

      res.send({
        token: getToken(savedUser),
        user: savedUser,
      })
    } else {
      res.status(409)
      res.send({
        message: 'User already exists',
      })
    }
  }
}

const login = async (req, res) => {
  console.log(req.body)
  const userFromDB = await UserModel.findOne({
    name: req.body.name,
  })

  if (userFromDB) {
    const isValidPassword = await bcrypt.compare(req.body.password, userFromDB.password)

    if (isValidPassword) {
      res.send({
        token: getToken(userFromDB),
        data: userFromDB,
      })
    } else {
      res.status(203)
      res.send({ message: 'invalid password' })
    }
  } else {
    res.status(203)
    res.send({ message: 'invalid user' })
  }
}

const getProfile = async (req, res) => {
  const decoded = authHelpers.decode(req.headers['authorization'])

  const userFromDB = await UserModel.findOne({
    _id: decoded._id,
  })

  if (userFromDB) {
    res.send(userFromDB)
  } else {
    res.status(404)
    req.send({ message: 'there is no such user' })
  }
}

const UserController = {
  save,
  login,
  getProfile,
}
export default UserController
