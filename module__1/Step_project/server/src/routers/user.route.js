import express from 'express'
import UserController from '../controllers/user.controller.js'

const UserRouter = express.Router()

UserRouter.post('/registration', UserController.save)
UserRouter.post('/login', UserController.login)

export default UserRouter
