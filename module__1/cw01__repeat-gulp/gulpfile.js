/******************* USE MODULES  *******************/
import gulp from 'gulp'
/******************* USE MODULES  *******************/




/******************* TASKS  *******************/
const buildHtml = async () => (
    gulp.src('./src/index.html')
        .pipe(gulp.dest('./build'))   
)
/******************* TASKS  *******************/



/******************* SETUP GULP TASKS  *******************/
gulp.task('default', buildHtml)
/******************* SETUP GULP TASKS  *******************/


