/******************* USE MODULES  *******************/
import gulp from 'gulp'
import concat  from 'gulp-concat'
import autoprefixer from 'gulp-autoprefixer'
import cleanCSS from 'gulp-clean-css'
import clean from 'gulp-clean'
/******************* USE MODULES  *******************/


/******************* PATHS  *******************/
const path = {
    src: {
        html: './src/index.html',
        css: './src/css/*.css',
        js: './src/index.js'
    },
    build: {
        self: './build',
        html: './build/',
        css: './build/css/',
        js: './build/js/',
    }
}
/******************* PATHS  *******************/


/******************* TASKS  *******************/
const buildHtml = async () => (
    gulp.src(path.src.html)
        .pipe(gulp.dest(path.build.html))   
)

const buildCss = async () =>
	gulp
		.src(path.src.css)
		.pipe(
			autoprefixer({
				cascade: false,
			}),
		)
		// .pipe(autoprefixer({cascade: false,}),) - ставить автопрефиксы для кроссбраузерности
		.pipe(concat('main.css'))
		// .pipe(concat('main.css')) - обьединяет файлы в один
		.pipe(cleanCSS({ compatibility: 'ie8' }))
		// .pipe(cleanCSS({ compatibility: 'ie8' })) - минимизирует файл до одной строки
		.pipe(gulp.dest(path.build.css))


const cleanBuild = () => (
    gulp.src(path.build.self, {read: false, allowEmpty: true})
        .pipe(clean())
)
/******************* TASKS  *******************/


/******************* SETUP GULP TASKS  *******************/
gulp.task('default', gulp.series(cleanBuild, buildHtml, buildCss))
/******************* SETUP GULP TASKS  *******************/