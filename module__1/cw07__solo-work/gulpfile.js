/******************* USE MODULES  *******************/
import gulp from 'gulp'
import concat from 'gulp-concat'
import autoprefixer from 'gulp-autoprefixer'
import cleanCSS from 'gulp-clean-css'
import clean from 'gulp-clean'
import imagemin from 'gulp-imagemin'
import uglify from 'gulp-uglify'
import browserSync from 'browser-sync'
import fileinclude from 'gulp-file-include'
/******************* USE MODULES  *******************/
browserSync.create()

/******************* PATHS  *******************/
const path = {
	src: {
		html: './src/pages/*.html',
		css: './src/css/*.css',
		js: './src/js/*.js',
		images: './src/images/**/*.*',
	},
	build: {
		self: './build',
		html: './build/',
		css: './build/css/',
		js: './build/js/',
		images: './build/images/',
	},
}
/******************* PATHS  *******************/

/******************* DESCRIPTION TASKS  *******************/
const buildHtml = async () =>
	gulp
		.src(path.src.html)
		.pipe(
			fileinclude({
				prefix: '@@',
				basepath: '@file',
			}),
		)
		.pipe(gulp.dest(path.build.html))
		.pipe(browserSync.stream())

const buildCss = async () =>
	gulp
		.src(path.src.css)
		.pipe(
			autoprefixer({
				cascade: false,
			}),
		)
		// .pipe(autoprefixer({cascade: false,}),) - ставить автопрефиксы для кроссбраузерности
		.pipe(concat('main.css'))
		// .pipe(concat('main.css')) - обьединяет файлы в один
		.pipe(cleanCSS({ compatibility: 'ie8' }))
		// .pipe(cleanCSS({ compatibility: 'ie8' })) - минимизирует файл до одной строки
		.pipe(gulp.dest(path.build.css))
		.pipe(browserSync.stream())

const buildJs = () =>
	gulp
		.src(path.src.js)
		.pipe(concat('main.js'))
		.pipe(uglify())
		// .pipe(uglify()) - gulp-uglify делаем файлы JS в одну строку
		.pipe(gulp.dest(path.build.js))
		.pipe(browserSync.stream())

const buildImages = () => gulp.src(path.src.images, { allowEmpty: true }).pipe(imagemin()).pipe(gulp.dest(path.build.images))

const cleanBuild = () => gulp.src(path.build.self, { read: false, allowEmpty: true }).pipe(clean())

const initialServer = async () => {
	browserSync.init({
		server: {
			baseDir: path.build.self,
		},
	})
	gulp.watch('./src/**/**/*.html', buildHtml).on('change', browserSync.reload)
	gulp.watch(path.src.css, buildCss).on('change', browserSync.reload)
	gulp.watch(path.src.js, buildJs).on('change', browserSync.reload)
}
/******************* DESCRIPTION TASKS  *******************/

/******************* SETUP GULP TASKS  *******************/
gulp.task('default', gulp.series(cleanBuild, buildHtml, buildCss, buildJs, buildImages, initialServer))
/******************* SETUP GULP TASKS  *******************/
