;(function changeTabs() {
	const paramsTabs = {
		action: { value: 4, url: '../images/action.jpg' },
		comedy: { value: 34, url: '../images/comedy.jpg' },
		drama: { value: 62, url: '../images/drama.jpg' },
		military: { value: 92, url: '../images/military.jpg' },
		fantasy: { value: 121, url: '../images/fantasy.png' },
	}

	const listTabs = document.querySelector('.hero__tabs')
	const listItems = document.querySelectorAll('.hero__tabs-item')

	const circle = document.querySelector('.hero__circle')
	const blockHero = document.querySelector('.hero')

	listTabs &&
		listTabs.addEventListener('click', function (event) {
			if (event.target.classList.contains('hero__tabs')) return null

			listItems.forEach(singleTab => {
				singleTab.classList.remove('active')
			})

			event.target.classList.add('active')

			const attribute = event.target.dataset.trigger

			circle.style.top = paramsTabs[attribute].value + 'px'
			blockHero.style.backgroundImage = `url(${paramsTabs[attribute].url})`

			localStorage.setItem('tabAttribute', attribute)
		})

	if (localStorage.getItem('tabAttribute')) {
		const localDataAttr = localStorage.getItem('tabAttribute')

		blockHero.style.backgroundImage = `url(${paramsTabs[localDataAttr].url})`

		listItems?.forEach(singleTab => {
			if (singleTab.dataset.trigger === localDataAttr) {
				singleTab.classList.add('active')
			}
		})
		circle.style.top = paramsTabs[localDataAttr].value + 'px'
	}
})()
