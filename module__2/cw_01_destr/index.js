// const data = [
// 	{
// 		price: 100,
// 		dateCreate: '12.12.2000',
// 		name: 'apple',
// 		isSale: false,
// 		isFavorite: false,
// 	},
// 	{
// 		price: 103333,
// 		dateCreate: '12.12.2000',
// 		name: 'apple123',
// 		isSale: false,
// 		isFavorite: false,
// 	},
// 	{
// 		price: 555,
// 		dateCreate: '12.12.2000',
// 		name: 'apple12312456',
// 		isSale: false,
// 		isFavorite: false,
// 	},
// ]

// example 1
// function render(fields) {
// 	const div = document.createElement('div')

// 	fields.forEach(singleIphone => {
// 		const { name, price } = singleIphone

// 		const h1 = document.createElement('h1')
// 		const text = document.createElement('p')

// 		h1.textContent = name
// 		text.textContent = price + '$'

// 		div.append(h1, text)
// 	})

// 	document.body.prepend(div)
// }

// render(data)

// example 2
// function render(fields) {
// 	const div = document.createElement('div')

// 	fields.forEach(({ name, price }) => {

// 		const h1 = document.createElement('h1')
// 		const text = document.createElement('p')

// 		h1.textContent = name
// 		text.textContent = price + '$'

// 		div.append(h1, text)
// 	})

// 	document.body.prepend(div)
// }

// render(data)

// const { isSale, name, salePrice = '20%' } = data

// console.log(salePrice)

// const array = ['nastya', 'oleg', 'vasya', 'vanya']

// console.log(array[0]);

// const [, , , , , , , , , , , player] = array

// console.log(player)

// array[2]

// function summ(a = 0, b = 0) {
// 	console.log(a + b)
// }

// summ()

const numbers = [11, 5, 3, 4, 22]

const otherNumbers = [1, 2, 3, 4]

// const newArray = numbers.concat(otherNumbers)
// const newArray = [...numbers, ...otherNumbers]

// console.log('🚀 ==== > newArray', newArray)

const statistic2020 = {
	personIll: 1000,
	personHealth: 3000,
	vacin: 400,
}

const statistic2021 = {
	personIll: 6000,
	personHealth: 5000,
}

const newStatistic = { ...statistic2020, ...statistic2021 }

// console.log(...numbers)

// function maxNumber(array) {
// 	let max = array[0]
// 	// debugger
// 	array.forEach(element => {
// 		if (max < element) {
// 			max = element
// 		}
// 	})

// 	return max
// }

// console.log(maxNumber(numbers))

// const maxN = Math.max(...numbers)

// console.log('🚀 ==== > maxN', maxN)

// const summ = (first, second, ...otherParams) => {
// console.log(otherParams)
// console.log(first)
// console.log(second)
// }

// summ(22, 44, 5, 6, 6, 7, 8, 8, 9, 2, 32, 324, 534, 5, 34, 5, 356, 54, 6)

// const data = ['vova', 'andret', 'egor']

// const copyData = { ...data }

// const obj = {
// 	age: 122,
// }

// const obj1 = {
// 	name: 'andrey',
// }

// const allObj = { ...obj, ...obj1 }

// obj.age
// obj['age']

// console.log(copyData['0'])
