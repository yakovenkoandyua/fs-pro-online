// TASK 1
/*
 * Две компании решили объединиться, и для этого им нужно объединить базу данных своих клиентов.
 * У вас есть 2 массива строк, в каждом из них - фамилии клиентов. Создайте на их основе один массив, который будет представлять * собой объединение двух массив без повторяющихся фамилий клиентов
 * */

const clients1 = ['Гилберт', 'Сальваторе', 'Пирс', 'Соммерс', 'Форбс', 'Донован', 'Беннет', 'Пирс']
const clients2 = ['Пирс', 'Зальцман', 'Сальваторе', 'Майклсон']

// const clientsAll = [...clients1, ...clients2]

// const dataClients = [...new Set(clientsAll)]

// Задание 2
// Перед вами массив characters, состоящий из объектов. Каждый объект описывает одного персонажа.
// Создайте на его основе массив charactersShortInfo, состоящий из объектов, в которых есть только 3 поля - name, lastName и age.

const characters = [
	{
		name: 'Елена',
		lastName: 'Гилберт',
		age: 17,
		gender: 'woman',
		status: 'human',
	},
	{
		name: 'Кэролайн',
		lastName: 'Форбс',
		age: 17,
		gender: 'woman',
		status: 'human',
	},
	{
		name: 'Аларик',
		lastName: 'Зальцман',
		age: 31,
		gender: 'man',
		status: 'human',
	},
	{
		name: 'Дэймон',
		lastName: 'Сальваторе',
		age: 156,
		gender: 'man',
		status: 'vampire',
	},
	{
		name: 'Ребекка',
		lastName: 'Майклсон',
		age: 1089,
		gender: 'woman',
		status: 'vempire',
	},
	{
		name: 'Клаус',
		lastName: 'Майклсон',
		age: 1093,
		gender: 'man',
		status: 'vampire',
	},
]

// example 1
// const charactersShortInfo = []
// for (let i = 0; i < characters.length; i++) {
// 	const info = {
// 		name: characters[i].name,
// 		lastName: characters[i].lastName,
// 		age: characters[i].age,
// 	}

// 	charactersShortInfo.push(info)
// }

// example 2
// const charactersShortInfo = characters.map(singleCharacter => {
// 	const info = {
// 		name: singleCharacter.name,
// 		lastName: singleCharacter.lastName,
// 		age: singleCharacter.age,
// 	}
// 	return info
// })
// example 2.1
// const charactersShortInfo = characters.map(singleCharacter => {
// 	const { name, lastName, age } = singleCharacter
// 	return {
// 		name: name,
// 		lastName: lastName,
// 		age: age,
// 	}
// })
// example 2.2
// const charactersShortInfo = characters.map(singleCharacter => {
// 	const { name, lastName, age } = singleCharacter
// 	return {
// 		name,
// 		lastName,
// 		age,
// 	}
// })
// example 2.3
// const charactersShortInfo = characters.map(({ name, lastName, age }) => ({
// 	name,
// 	lastName,
// 	age,
// }))

// console.log('🚀 ==== > charactersShortInfo', charactersShortInfo)

// TASK 3
/*
 *сделать деструктуризаю обьекта так чтобы свойство
 * name принадлежало пременной name,
 * свойство years принадлежало пременной age,
 *  свойство isAdmin было по умолчанию false
 */
const user = {
	name: 'John',
	years: 30,
}

// TASK 4
// Получить сведения о приступнике за послдение 3 года таким образом, чтобы свойства в итоговом обьекте были актуальны по годам
const satoshi2020 = {
	name: 'Nick',
	surname: 'Sabo',
	age: 51,
	country: 'Japan',
	birth: '1979-08-21',
	location: {
		lat: 38.869422,
		lng: 139.876632,
	},
}

const satoshi2019 = {
	name: 'Dorian',
	surname: 'Nakamoto',
	age: 44,
	hidden: true,
	country: 'USA',
	wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
	browser: 'Chrome',
}

const satoshi2018 = {
	name: 'Satoshi',
	surname: 'Nakamoto',
	technology: 'Bitcoin',
	country: 'Japan',
	browser: 'Tor',
	birth: '1975-04-05',
}
