const mailStorage = [
	{
		subject: 'Hello world',
		from: 'gogidoe@somemail.nothing',
		to: 'lolabola@ui.ux',
		text: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?',
	},
	{
		subject: 'How could you?!',
		from: 'ladyboss@somemail.nothing',
		to: 'ingeneer@nomail.here',
		text: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?',
	},
	{
		subject: 'Acces denied',
		from: 'info@cornhub.com',
		to: 'gogidoe@somemail.nothing',
		text: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?',
	},
]

window.addEventListener('DOMContentLoaded', () => {
	const parent = document.querySelector('.emails')
	showLetter(mailStorage, parent)

	const loader = document.querySelector('.loader')

	setTimeout(() => {
		parent.style.opacity = 1
		loader.style.opacity = 0
	}, 2000)
})

function showLetter(data, container) {
	data.forEach(function (item) {
		const wrapper = document.createElement('div')
		wrapper.className = 'email-item'

		// EXAMPLE 2
		// const titleSubject = createComponent('h4', item.subject, { className: 'email-subject' })
		// const linkFrom = createComponent('a', item.from, {
		// 	className: 'email-subtext email-from',
		// 	href: item.from,
		// })
		// const linkTo = createComponent('a', item.to, {
		// 	className: 'email-subtext email-to',
		// 	href: item.to,
		// 	id: 'root-link'
		// })
		// const textLetter = createComponent('p', item.text, { className: 'email-text' })

		const titleSubject = createComponent('h4', 'email-subject', item.subject)
		const linkFrom = createComponent('a', 'email-subtext email-from', item.from, item.from)
		const linkTo = createComponent('a', 'email-subtext email-to', item.to, item.to)
		const textLetter = createComponent('p', 'email-text', item.text, true)
		const deleteLeter = createComponent('span', 'email-delete', 'x')

		const wrapperLink = document.createElement('div')
		wrapperLink.className = 'email-subtext-wrapper'

		wrapperLink.append(linkFrom, linkTo)

		wrapper.append(titleSubject, wrapperLink, textLetter, deleteLeter)

		container.append(wrapper)

		wrapper.addEventListener('click', event => {
			textLetter.hidden = !textLetter.hidden

			if (event.target.classList.contains('email-delete')) {
				event.currentTarget.remove()
			}
		})
	})
}

function createComponent(tag, classValue, content, attribute = '') {
	const element = document.createElement(tag)
	element.className = classValue
	element.textContent = content

	if (attribute !== '') {
		element.href = `mailto: ${attribute}`
	}

	if (tag === 'p') {
		element.hidden = attribute
	}

	return element
}
