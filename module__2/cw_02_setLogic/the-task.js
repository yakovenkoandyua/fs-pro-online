// There is some `mailStorage` - a little simulation of real storage with email letters.
const mailStorage = [
	{
		subject: 'Hello world',
		from: 'gogidoe@somemail.nothing',
		to: 'lolabola@ui.ux',
		text: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?',
	},
	{
		subject: 'How could you?!',
		from: 'ladyboss@somemail.nothing',
		to: 'ingeneer@nomail.here',
		text: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?',
	},
	{
		subject: 'Acces denied',
		from: 'info@cornhub.com',
		to: 'gogidoe@somemail.nothing',
		text: 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?',
	},
]

window.addEventListener('DOMContentLoaded', () => {
	const wrapperMail = document.querySelector('.emails')
	const loader = document.querySelector('.loader')

	wrapperMail.addEventListener('click', handleText)

	renderMails(mailStorage, wrapperMail)

	setTimeout(() => {
		loader.style.opacity = 0
		wrapperMail.style.opacity = 1
	}, 2000)
})

function handleText(event) {
	if (event.target === event.currentTarget) return null
	// if (event.target.classList.contains('emails')) return null
	if (event.target.classList.contains('email-delete')) return null

	if (event.target.classList.contains('email-item')) {
		const text = event.target.querySelector('.email-text')
		text.hidden = !text.hidden
	} else {
		const item = event.target.closest('.email-item') // наше письмо то есть див
		const text = item.querySelector('.email-text')
		text.hidden = !text.hidden
	}
}

const removeMail = mail => mail.remove()

function renderMails(emails, wrapper) {
	emails.forEach(singleEmail => {
		const { subject, from, to, text } = singleEmail

		const itemBlock = createElement('div', 'email-item')
		const title = createElement('h1', 'email-subject', subject)
		const linkFrom = createElement('a', 'email-from', from)
		const linkTo = createElement('a', 'email-to', to)
		const desc = createElement('p', 'email-text', text)
		const deleteMail = createElement('button', 'email-delete', 'X')

		deleteMail.addEventListener('click', () => removeMail(itemBlock))

		itemBlock.append(title, linkFrom, linkTo, desc, deleteMail)

		wrapper.append(itemBlock)

		// wrapper.insertAdjacentHtml('beforeend', `
		// <div class="email-item">
		// 	<h1>${subject}</h1>
		// </div>
		// `)
	})
}

function createElement(tag, classes, attribute = '') {
	const element = document.createElement(tag)
	element.className = classes
	element.textContent = attribute

	if (tag === 'a') {
		element.href = `mailto: ${attribute}`
	}
	if (tag === 'p') {
		element.hidden = true
	}

	return element
}

// let num = 30

// const h1 = document.createElement('h1')
// document.body.prepend(h1)
// setInterval(() => {
// 	h1.textContent = num
// 	num--
// }, 1000)
