class Human {
	constructor(props) {
		const { gender, age, nation } = props
		this.gender = gender
		this.age = age
		this.nation = nation
	}
	// constructor({ gender, age, nation }) {
	// 	this.gender = gender
	// 	this.age = age
	// 	this.nation = nation
	// }
}

class Employe extends Human {
	constructor(props, job) {
		super(props)
		this.job = job
	}

	goToWork() {
		setTimeout(() => {
			alert('go to work')
		}, 36000000)
	}
}

class Actor extends Human {
	constructor(props, film) {
		super(props)
		this.film = film
	}
}

const humanData = { gender: 'male', age: '28', nation: 'ukraine' }

const ukrainene = new Human(humanData)
console.log('🚀 ==== > ukrainene', ukrainene)

const loyer = new Employe('male', '43', 'england', 'loyer')
console.log('🚀 ==== > loyer', loyer)

// const poland = new Human('female', '28', 'poland')
// console.log('🚀 ==== > poland', poland)
