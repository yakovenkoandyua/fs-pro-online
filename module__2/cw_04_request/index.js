const baseURL = 'https://api.ipify.org/?format=json'

const queryIP = query => 'http://ip-api.com/json/' + query

const btn = document.getElementById('send')

const address = document.querySelector('.address')

btn.addEventListener('click', function () {
	fetch(baseURL)
		.then(res => res.json())
		.then(result => {
			const { ip } = result

			const urlIp = queryIP(ip)
			// const urlIp = 'http://ip-api.com/json/' + ip
			// const urlIp = `http://ip-api.com/json/${ip}?fields=status,message,country,regionName,city,district,org,query`

			fetch(urlIp)
				.then(res => res.json())
				.then(location => {
					console.log(location)
					address.insertAdjacentHTML(
						'afterbegin',
						`
                        місто - ${location.city}
                        </br>
                        країна - ${location.country}
                        </br>
                        мережа користування - ${location.org}
                    `,
					)
				})
		})
})
