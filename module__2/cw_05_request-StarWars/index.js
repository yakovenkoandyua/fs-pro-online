window.addEventListener('DOMContentLoaded', () => {
	getFilms()
})

class CreateElement {
	constructor(tag, content, container, classes) {
		this.tag = tag
		this.content = content
		this.container = container
		this.classes = classes || null
	}

	render() {
		const element = document.createElement(this.tag)

		element.textContent = this.content

		if (this.classes) element.className = this.classes
		//  element.className = this.classes ? this.classes : ''

		this.container.append(element)
	}
}

function getFilms() {
	const baseUrl = 'https://swapi.dev/api/films'

	const wrapper = document.querySelector('.list')
	const container = document.querySelector('.wrapper')
	const loader = document.querySelector('.loader')

	fetch(baseUrl)
		.then(res => res.json())
		.then(({ results }) => {
			// const { results } = dataResult
			results.forEach((singleFilm, id) => {
				const { director, title, characters, opening_crawl } = singleFilm

				const listItem = document.createElement('div')
				listItem.className = 'list-item'

				const listTitle = new CreateElement('h1', title, listItem, 'list-title').render()
				// listTitle.render()
				const listDirector = new CreateElement('h3', director, listItem, 'list-director').render()
				// listDirector.render()
				const listDesc = new CreateElement('p', opening_crawl, listItem, 'list-desc').render()
				// listDesc.render()
				const listBtnCharacters = new CreateElement('button', 'show characters', listItem, 'list-btn').render()
				// listBtnCharacters.render()

				wrapper.append(listItem)

				getAllCharacters(characters, listItem, id)
			})

			loader.hidden = !loader.hidden
			container.hidden = !container.hidden
		})
}

function getAllCharacters(dataUrls, container, index) {
	console.log(dataUrls)

	const showMore = document.querySelectorAll('.list-btn')

	const listHero = document.createElement('ul')
	listHero.className = 'list-hero'

	showMore[index].addEventListener('click', () => {
		dataUrls.forEach(heroUrl => {
			fetch(heroUrl)
				.then(res => res.json())
				.then(({ name }) => {
					const li = document.createElement('li')
					li.textContent = name
					listHero.append(li)
				})
		})

		container.append(listHero)
	})
}
