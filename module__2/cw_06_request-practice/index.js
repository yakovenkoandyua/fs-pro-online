// window.addEventListener('DOMCOntentLoaded', () => {})

const submit = document.getElementById('submit')

submit.addEventListener('click', e => {
	e.preventDefault()

	const count = document.getElementById('post').value

	fetch(`https://jsonplaceholder.typicode.com/posts?_limit=${count}`)
		.then(response => response.json())
		.then(data => {
			const root = document.getElementById('root')

			data.forEach(({ title, body, userId, id }) => {
				const item = createElem('div')
				const itemTitle = createElem('h2', title)
				const itemDesc = createElem('p', body)
				const itemDelete = createElem('button', 'x')

				item.append(itemTitle, itemDesc, itemDelete)

				root.append(item)
			})
		})
})

function createElem(tag, content = '') {
	const el = document.createElement(tag)

	el.textContent = content

	return el
}
