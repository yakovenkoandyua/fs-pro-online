import path from 'path'
import TwitsModel from '../models/twits.model.js'

const __dirname = path.resolve()

const getHomepage = (req, res) => {
	res.sendFile(path.join(__dirname + '../../client/index.html'))
}

const getTwits = async (request, response) => {
	const allTwits = await TwitsModel.find()

	if (allTwits) {
		response.send(allTwits)
	} else {
		response.status(404)
		response.send({ message: 'posts is not created' })
	}
}

const addTwits = async (request, response) => {
	const newTwit = await TwitsModel.create({ ...request.body })

	return response.send({ newTwit, message: 'Twit added' })
}

const removeTwits = (request, response) => {
	const id = request.params.id

	const deletedTwit = TwitsModel.findByIdAndDelete(id)

	response.send({ message: 'twit deleted', deletedTwit })
}

const TwitsController = {
	getHomepage,
	getTwits,
	addTwits,
	removeTwits,
}

export default TwitsController
