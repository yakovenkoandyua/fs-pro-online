import express  from "express";
import TwitsController from '../controller/twits.controller.js'

const TwitsRouter = express.Router()

TwitsRouter.get('/', TwitsController.getHomepage)
TwitsRouter.get('/twits', TwitsController.getTwits)
TwitsRouter.post('/twits', TwitsController.addTwits)
TwitsRouter.delete('/twits/:id', TwitsController.removeTwits)


export default TwitsRouter



